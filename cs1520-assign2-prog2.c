/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	Name: Akshay Bansal
	Roll No: CS1520
	Date of Submission: 30/12/2015
	Date of deadline: 4/1/2015
	Program description: Searching for audio/video files with given constraints
	Acknowledgments: The C Programming Language(K&R), http://apt-browse.org/browse/ubuntu/trusty/main/amd64/libtagc0-dev/1.9.1-2/file/usr/share/doc/libtagc0-dev/examples/tagreader_c.c
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <dirent.h>
	#include <sys/types.h>
	#include <ctype.h>
	#include <taglib/tag_c.h>
	#define ERR_MESG(x) { printf(x); exit(1); }
	#ifndef FALSE
	#define FALSE 0
	#endif
	
	int count;

	typedef struct node{
		char *fpath;
		struct node *next;
	}NODE;

	typedef struct{
		NODE *head;
		int count;
	}STACK;

	/* To initialize a STACK */
	STACK * init();

	/* To find out if a STACK is empty */
	int isEmpty(STACK *);

	/* To push a node into the STACK */
	void push(STACK *, char *);

	/* To pop from the STACK */
	void pop(STACK *);

	/* To find the info stored in the topmost node of the STACK */
	char *top(STACK *,int);

	/* To find out the audio/video files satisfying the user constraints */
	void avsearch(char*, char**, int);

	/* To convert the string to lowercase */ 
	void strlower(char *);

	int main(int argc, char **argv){

		STACK *S;
		int len_name,len_path,index,usage=1;
		char *path,*name,*ptr;
		DIR *dirp;
		struct dirent *dptr;
		count=0;		

		if(argc%2!=0)
			usage=0;

		index=1;
		while(index<argc-2 && usage){
			if(argv[index][0]!='-' || strlen(argv[index])!=2){
				usage=0;
				break;
			}
			switch(argv[index][1]){
				case 'a': strlower(argv[index+1]);
							break;
				case 't': strlower(argv[index+1]);
							break;
				case 'd': ptr=argv[index+1]; while((*ptr))	{ if(!isdigit(*(ptr++))) {usage=0; break;} }
							break;
				default: printf("%s: invalid option -- '%c'\n",argv[0],argv[index][1]); usage=0;
			}
			index+=2;
		}
		if(!usage){
			printf("Usage: %s [OPTIONS] [PATTERN] DIRECTORY_PATH\n",argv[0]);
			printf("\nOPTIONS:\n-t\t:\tDisplay files with matched string in title\n-a\t:\tDisplay files with matched string in artist name\n-d\t:\tDisplay audio/video files with duration atmost given time in seconds\n");
			exit(1);
		}	
		
		S=init();//Intialize the STACK

		/* Implementation of Depth-First Search Algorithm to list out the path of all files and directories rooted at the directory given as an argument and simultaneously checking for the constraints on the valid audio/video files */
		len_name=strlen(argv[argc-1]);
		name=(char*)calloc(len_name+1,sizeof(char));
		if(name==NULL)
			ERR_MESG("OUT OF MEMORY\n")
		strcpy(name,argv[argc-1]);
		push(S,name);
		while(!isEmpty(S)){
			len_path=strlen(S->head->fpath);
			path=top(S,len_path);
			pop(S);
			if((dirp=opendir(path))!=NULL){
				while((dptr=readdir(dirp))!=NULL){
					if(strcmp(dptr->d_name,"..") && strcmp(dptr->d_name,".")){
						len_name=strlen(dptr->d_name)+len_path+1;
						name=(char*)calloc(len_name+1,sizeof(char));
						if(name==NULL)
							ERR_MESG("OUT OF MEMORY\n")
						strcpy(name,path);
						strcat(name,"/");
						strcat(name,dptr->d_name);
						if(dptr->d_type==DT_REG || dptr->d_type==DT_UNKNOWN){
							if(argc!=2)
								avsearch(name,argv,argc);
							else{
								printf("%s\n",name);
								count++;
							}
						}
						else
							push(S,name);
					}
				}
				free(dirp);			
			}
			free(path);
		}
		printf("No. of files found: %d\n",count);
		free(S);

		return(0);
	}

	void avsearch (char *filepath, char **argv, int argc){
	
		char *artist_name,*title_song,ch;
		int duration_song,index,found=1;
		TagLib_File *file;
		TagLib_Tag *tag;
		const TagLib_AudioProperties *properties;

		taglib_set_strings_unicode(FALSE);
		file = taglib_file_new(filepath);

		/* Limiting the search to audio/video files */
		if(file == NULL)
			return;

		tag = taglib_file_tag(file);
		properties = taglib_file_audioproperties(file);
		
		if(tag!=NULL){
			title_song=taglib_tag_title(tag);
			strlower(title_song);
			artist_name=taglib_tag_artist(tag);
			strlower(artist_name);
		}
		else{
			title_song=NULL;
			artist_name=NULL;
		}	
		if(properties!=NULL)
			duration_song=taglib_audioproperties_length(properties);
		else
			duration_song=-1;

		/* Checking for constraints given as arguments */
		index=1;
		while(index < argc-2 && found){
			ch=argv[index][1];
			switch(ch){
				case 'a': if(artist_name==NULL || strstr(artist_name,argv[index+1])==NULL) found=0;
							break;
				case 't': if(title_song==NULL || strstr(title_song,argv[index+1])==NULL) found=0;
							break;
				case 'd': if(duration_song<0 || duration_song>atoi(argv[index+1])) found=0;
							break;
			}
			index+=2;
		}	
		if(found){
			printf("%s\n",filepath);
			count++;
		}	
		taglib_tag_free_strings();
		taglib_file_free(file);

		return;
	}

	void strlower(char *s){

		int length,i;
		length=strlen(s);
		for(i=0;i<length;i++){
			if(s[i]>=65 && s[i]<=90)
				s[i]=tolower(s[i]);
		}
		return;
	}
			

	STACK * init(){

		STACK *S;
		S=(STACK*)malloc(sizeof(STACK));
		S->head=NULL;
		S->count=0;
		
		return(S);
	}

	int isEmpty(STACK *S){

		if((S->head)==NULL)
			return(1);
		else
			return(0);
	}

	void push(STACK *S,char *path){

		NODE *new_node;
		new_node=(NODE*)malloc(sizeof(NODE));
		if(new_node==NULL)
			ERR_MESG("OUT OF MEMORY\n")
		new_node->fpath=path;
		new_node->next=S->head;
		S->head=new_node;
		(S->count)++;
		return;

	}


	void pop(STACK *S){		

		NODE *temp_node;

		temp_node=(S->head);	
		S->head=(S->head)->next;
		free(temp_node->fpath);
		free(temp_node);		
		(S->count)--;

		return;
	}

	char *top(STACK *S, int len_fpath){

		char *info;
		info=(char*)calloc(len_fpath+1,sizeof(char));
		if(info==NULL)
			ERR_MESG("OUT OF MEMORY\n")
		strcpy(info,S->head->fpath);
		return(info);
	}

