/*------------------------------------------------------------------------------------------------------------------
	Name: Akshay Bansal
	Roll no: CS1520
	Date of submission: 30/12/2015
	Date of deadline: 4/1/2016
	Program description: Formatting a given unformatted-text
	Acknowledgments: The C Programming Language(K&R)
------------------------------------------------------------------------------------------------------------------*/

	#include <stdio.h>
	#include <stdlib.h>
	#include <ctype.h>
	#define ERR_MESG(x) { printf(x); exit(1); }
	#define N 79  //To limit the max. # characters in a single line to 'N+1' 
	#define IN 1  //state=IN if currently inside a paragraph
	#define OUT 0 //state=OUT if currently outside a paragraph

	typedef struct node{
		char letter;
		struct node *next;
	}NODE;

	typedef struct{
		NODE *front;
		NODE *end;
		int count;
	}QUEUE;

	/* Initializing queue that stores the current word */
	QUEUE *init();
	
	/* To enqueue a character of the current word in the initialized queue */
	void enqueue(QUEUE *, char);

	/* To write the queue of characters(word) in the output file */
	void writeQ(FILE *, QUEUE *);

	/* To delete the list of characters(word) in the queue */
	void freeQ(QUEUE *);

	/* To form a queue of characters constituting the current word and return its length */
	int wordlength_count(FILE *, QUEUE *);

	/* To find the appropriate position in the output file where the enqueued word is written according to the proposed constraints and return the no. of empty positions left in the current line */
    	int format_write(FILE *, int, int, QUEUE *);

	/* To determine if a given character belongs to one of the 'punctuation characters' mentioned in the specified constraint */ 
    	int ispunc(char);

	int main(int argc, char **argv){

		int length, nchar=N,state,nline=0,start;
		char ch;
		FILE *fp_in,*fp_out;
		QUEUE *Q;		

		if(argc!=2){
			printf("Usage: %s FILE \n",argv[0]);
			exit(1);
		}

		fp_in=fopen(argv[1],"r");
		if(fp_in==NULL)
			ERR_MESG("Error: input file cannot be read\n")
		fp_out=fopen("output.txt","w");
		if(fp_out==NULL)
			ERR_MESG("Error: output file cannot be created\n")

		Q=init();//Initializes a queue
		state=OUT;//Initial state = OUT

		while((ch=fgetc(fp_in))!=EOF){
			/* If a 'newline' character has been encountered, it checks for subsequent characters for another 'newline' or (!isspace(ch)) on the basis of which 'state' is determined */
        		if(nline){
				if(isspace(ch)){
					if(ch=='\n')
            					state=OUT;
				}
				else
					nline=0;				
			}
			/* If state is OUT and !isspace(ch)==1, new paragraph is started */
            		if(state==OUT){
            			if(!isspace(ch)){
                    			state=IN;
                    			fputc('\n',fp_out);
					fputc('\n',fp_out);
					nchar=N;
					start=1;
                		}
            		}
			/* If state==IN and 'newline' is encountered, nline=1 else if isspace(ch)!=1, word is enqueued in the initialized queue */
            		if(state==IN){
                		if(ch=='\n')
                    			nline=1;
				else{ 
					if(!isspace(ch)){
                				enqueue(Q,ch);
                				length=wordlength_count(fp_in,Q);
						if(start){
							writeQ(fp_out,Q);
							nchar-=length;
							start=0;
						}
						else
                					nchar=format_write(fp_out,length,nchar,Q);
            				}
				}   
           		}
        	}

		fclose(fp_in);
		fclose(fp_out);
		free(Q);

		return(0);

	}

	int wordlength_count(FILE *fp, QUEUE *Q){

		char ch;
		while(!isspace(ch=fgetc(fp)))
			enqueue(Q,ch);

		return(Q->count);
	}

	int format_write(FILE *fp, int length, int nchar, QUEUE *Q){

		char ch;
		/* If length(word)==1 and belongs to the allowed punctuation characters, it is put next to last word(without whitespace) */
		if(length==1 && ispunc(ch=Q->front->letter)){
        		fputc(ch,fp);
			freeQ(Q);
			if(nchar>=1)
            			return(nchar-1);
			else
				return(0);
        	}
		/* If length(word)>=nchar(no. of non-filled empty characters) + 1(space to separate the word) */
		if(nchar >= length+1){
			fputc(' ',fp);
			writeQ(fp,Q);
			return(nchar-length-1);
			
		}
		/* If previous condition does not hold true, new word is written on the next line else if word(length)>N+1, new word is written completely on the line with return value of nchar=0 */ 
		if(length<=N){
			fputc('\n',fp);
			writeQ(fp,Q);
			return(N-length);
		}
		else{
			fputc('\n',fp);
			writeQ(fp,Q);
			return(0);
		}
		
	}

	int ispunc(char ch){
	
		if(ch==',' || ch==';' || ch=='.' || ch=='?' || ch=='!' || ch==':')
			return(1);
		else
			return(0);
	}

	QUEUE *init(){
		QUEUE *Q;
		Q=(QUEUE*)malloc(sizeof(QUEUE));
		Q->front=NULL;
		Q->end=NULL;
		Q->count=0;
	
		return(Q);
	}


	void enqueue(QUEUE *Q, char ch){

        	NODE *new_node;
		/* Creating a new node and assigning charaters as its info */
        	new_node=(NODE*)malloc(sizeof(NODE));
		new_node->letter=ch;
		new_node->next=NULL;
		(Q->count)++;
		
		/* process to add new node when queue is empty */
		if(Q->front == NULL){
			Q->front=new_node;
			Q->end=new_node;
			return;
		}
		/* process to add new node(shifting rear node) when queue is non-empty */
		Q->end->next=new_node;
		Q->end=new_node;
		return;

  	}
	
	void writeQ(FILE *fp, QUEUE *Q){

		NODE *curr_node;
		curr_node=Q->front;
		
		while(curr_node!=NULL){
			fputc(curr_node->letter,fp);
			curr_node=curr_node->next;
		}
		freeQ(Q);
		return;
	}

	void freeQ(QUEUE *Q){

		NODE *temp_node;

		/* deleting elements of the queue */
		while(Q->front!=NULL){
			temp_node=Q->front;
			Q->front=Q->front->next;
			free(temp_node);
		}
		Q->count=0;
		
		return;
	}


		
	

		

		
